package com.itb.calendar.presentation.screens.dayofweek;

import androidx.lifecycle.ViewModelProviders;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavDirections;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.itb.calendar.R;
import com.itb.calendar.domain.MpSession;

import java.util.List;

public class DayOfWeekFragment extends Fragment {

    private DayOfWeekViewModel mViewModel;
    MpSession mpSession;
    RecyclerView recyclerView;

    public static DayOfWeekFragment newInstance() {
        return new DayOfWeekFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.main_fragment, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
//        view.setOnClickListener(this::changeFragment);
        recyclerView = view.findViewById(R.id.recyclerView);

        recyclerView.setHasFixedSize(true);

        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(layoutManager);

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = ViewModelProviders.of(this).get(DayOfWeekViewModel.class);

        mViewModel.loadSessions();
        List<MpSession> mpSessions = mViewModel.getMpSessions();

        DayOfWeekAdapter dayOfWeekAdapter = new DayOfWeekAdapter(mpSessions);
        dayOfWeekAdapter.setOnMpClickListener(this::onMpClicked);
        recyclerView.setAdapter(dayOfWeekAdapter);
    }


    public void onMpClicked(MpSession mpSession, View view) {
        Toast.makeText(getContext(), mpSession.getMp().getName(), Toast.LENGTH_LONG).show();
        NavDirections action = DayOfWeekFragmentDirections.actionToFragmentDetail(mpSession.getMp().getNumber());
        Navigation.findNavController(view).navigate(action);
    }
}
