package com.itb.calendar.repository.data;

import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.itb.calendar.domain.MpSession;
import com.itb.calendar.repository.data.MpData;

/**
 * Stores session data
 */
public class SessionData {
    Map<Integer, List<MpSession>> sessions;
    MpData mpData;

    public SessionData(MpData mpData) {
        this.mpData = mpData;
        createData();
    }

    public Map<Integer, List<MpSession>> getSessions() {
        return sessions;
    }

    private void createData(){
        sessions = new HashMap();
        sessions.put(Calendar.MONDAY, createMondayData());
        sessions.put(Calendar.TUESDAY, createTuesdayData());
        sessions.put(Calendar.WEDNESDAY, createWednesdayData());
        sessions.put(Calendar.THURSDAY, createThursdayData());
        sessions.put(Calendar.FRIDAY, createFridayData());
    }

    private List<MpSession> createMondayData() {
        return Arrays.asList(
                new MpSession(mpData.getMp(9), "15:00", "16:50"),
                new MpSession(mpData.getMp(17), "16:50", "20:05"),
                new MpSession(mpData.getMp(8), "20:05", "21:00")
        );
    }

    private List<MpSession> createTuesdayData() {
        return Arrays.asList(
                new MpSession(mpData.getMp(7), "15:00", "17:45"),
                new MpSession(mpData.getMp(16), "18:15", "21:00")
//                new MpSession(mpData.getMp(9), "17:00", "21:00")
        );
    }

    private List<MpSession> createWednesdayData() {
        return Arrays.asList(
                new MpSession(mpData.getMp(17), "15:00", "15:55"),
                new MpSession(mpData.getMp(8), "15:55", "19:10"),
                new MpSession(mpData.getMp(9), "19:10", "20:05"),
                new MpSession(mpData.getMp(0), "20:05", "21:00")
        );
    }

    private List<MpSession> createThursdayData() {
        return Arrays.asList(
                new MpSession(mpData.getMp(1), "15:00", "15:55"),
                new MpSession(mpData.getMp(10), "15:55", "17:45"),
                new MpSession(mpData.getMp(2), "18:15", "21:00")
        );
    }

    private List<MpSession> createFridayData() {
        return Arrays.asList(
                new MpSession(mpData.getMp(17), "15:00", "17:45")
//                new MpSession(mpData.getMp(8), "15:55", "17:00"),
//                new MpSession(mpData.getMp(9), "17:00", "21:00")
        );
    }

    public MpData getMpData() {
        return mpData;
    }
}
